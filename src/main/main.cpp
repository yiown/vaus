#include <GLFW/glfw3.h>
#include <iostream>
#include <App.h>

/**
 * @brief The application.
 */
static App app;

/**
 * @brief Error handler.
 * @param error Error code.
 * @param description Error description.
 */
static void ErrorCallback(int error, const char* description) {
    std::cerr << "ERROR " << error << " : " << description << std::endl;
}

/**
 * @brief Key event callback.
 * @param window Window reference.
 * @param key Key code.
 * @param scancode Scan code.
 * @param action Action code.
 * @param mods Mods flags.
 */
static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    app.keyEvent(window, key, scancode, action, mods);
}

/**
 * @brief Handler for FB size change.
 * @param window Window reference.
 * @param width New width.
 * @param height New height.
 */
static void FramebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
}

/**
 * @brief Main entry point.
 * @param argc Argument count.
 * @param argv Arguments.
 * @return Exit status.
 */
int main(int argc, char** argv) {
    // initialize GLFW
    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit()) {
        return EXIT_FAILURE;
    }

    // setup window
    GLFWwindow* window = glfwCreateWindow(640, 480, app.name().c_str(), nullptr, nullptr);
    if (!window) {
        glfwTerminate();
        return EXIT_FAILURE;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    // setup OpenGL
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    FramebufferSizeCallback(window, width, height);

    // main loop
    if(argc < 2 || !app.init(argv[1]))
        return EXIT_FAILURE;
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);
        app.update();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    app.shutdown();

    // clean up
    glfwDestroyWindow(window);
    glfwTerminate();
    return EXIT_SUCCESS;
}
