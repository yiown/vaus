#ifndef APP_H
#define APP_H
#include <string>
#include <vector>
#include <GLFW/glfw3.h>
#include <Rect.h>

class App {
private:
    std::vector<Rect> rects;

public:
    App();
    virtual ~App();

    std::string name();

    bool init(std::string filename);
    void update();
    void shutdown();

    void keyEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
};

#endif // APP_H
