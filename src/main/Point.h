#ifndef POINT_H
#define POINT_H

class Point {
public:
    float x, y;

    Point(float x, float y) : x(x), y(y) {}
};

#endif // POINT_H
