#include <App.h>
#include <iostream>
#include <fstream>

App::App(){
}

App::~App(){
}

std::string App::name() {
    return "Vaus";
}

bool App::init(std::string filename) {
    // open file
    std::ifstream file(filename);
    if(!file)
        return false;

    // parse map
    float n[5];
    while(!file.eof()) {
        int i;
        for(i=0;i<5;i++)
            if(!(file >> n[i]))
                break;
        if(i==5)
            rects.push_back(Rect{Point{n[0], n[1]}, Point{n[2], n[3]}, Color{n[4]}});
    }
    std::cout << "Map: " << filename << " Rects: " << rects.size() << std::endl;
    return true;
}

void App::update() {
    for(Rect r : rects)
        r.draw();
}

void App::shutdown() {
}

void App::keyEvent(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}
