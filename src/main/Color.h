#ifndef COLOR_H
#define COLOR_H

class Color {
public:
    float r, g, b;

    Color(float r, float g, float b) : r(r), g(g), b(b) {}
    Color(int i) : r(i&0xFF0000), g(i&0xFF00), b(i&0xFF) {}
};

#endif // COLOR_H
