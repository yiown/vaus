#ifndef RECT_H
#define RECT_H

#include <Point.h>
#include <Color.h>

class Rect {
public:
    Point a, b;
    Color c;

    Rect(Point a, Point b, Color c) : a(a), b(b), c(c) {}

    void draw();
};

#endif // RECT_H
